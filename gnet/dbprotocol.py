# -*- coding: utf-8 -*-
'''database protocol & factory
: 和protocol的factory不同, 这里 reconnect_delay全部作为__init__参数
'''

import gevent
import logging
from . db.gmy import Pool

logger = logging.getLogger(__name__)


class MySQLClientPoolFactory:
    '''mysql client pool factory
    : 不在新协程中执行
    '''
    
    def __init__(self, kwargs, nglets, reconnect_delay):
        self.options = (kwargs, nglets, reconnect_delay)
        
    def start(self):
        self._dbpool = dbpool = Pool(*self.options)
        self.query = dbpool.query
        self.execute = dbpool.execute
        self.fetchone = dbpool.fetchone
        self.fetchall = dbpool.fetchall
        self.get_fields = dbpool.get_fields
        logger.info('mysql client pool connect to %s', str(self.options))
