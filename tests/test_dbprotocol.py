# -*- coding: utf-8 -*-
from gnet.dbprotocol import MySQLClientPoolFactory
import gevent
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='[%(asctime)-15s %(levelname)s:%(module)s] %(message)s')
        
f = MySQLClientPoolFactory(dict(host='127.0.0.1', user='root', passwd='112358', db='test', port=3306), 10, 0)
f.start()

print f.fetchall("select * from book")

gevent.wait()
