# -*- coding: utf-8 -*-
from gnet.rpcprotocol import MPRPCClientFactory
import gevent
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='[%(asctime)-15s %(levelname)s:%(module)s] %(message)s')
        
f = MPRPCClientFactory(('127.0.0.1', 6021))
f.start()

print f.call('sum', 1, 2)

gevent.wait()
